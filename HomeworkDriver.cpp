/**
 * @file HomeworkDriver.cpp
 * @author James R. Daehn
 * @brief Main demo/test driver for homework assignment #3.
 */
#define FALSE 0
#define TRUE !FALSE
#define FIDDLING FALSE

#if FIDDLING

#include <cstdlib>
#include <iostream>
#include <string>
#include "WordParser.h"

int main(int argc, char** argv) {
	std::string s;
	std::cout << "Enter a string: ";
	std::getline(std::cin, s);
	// In a windows environment, "new lines" are stored as two characters: \r\n
	// In *nix environments, including Mac OS X, "new lines" are stored as one character: \n
	// When processing our strings, we do not want to include the new line character(s)
	// Since all string functions "ignore" the \n, the lines read using getline effectively
	// end in \r in Windows environments, or nothing in *nix environments. Let's see what we
	// have and pass the appropriate string to isWord:

	// Initialize endPosition to end of the string
	size_t endPosition { s.length() };
	// If the end of the string is the carriage return...
	if (s[endPosition == '\r']) {
		// mark the end of the string as 1 less than the length of the string
		endPosition -= 1;
	}

	// Report whether the read string is in our language or not
	WordParser wordParser;
	std::cout << "Does the string match our language? "
			<< (wordParser.isWord(s.substr(0, endPosition)) ? "yes" : "no")
			<< std::endl;

	return EXIT_SUCCESS;
}

#else

#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>

#include <cppunit/Test.h>
#include <cppunit/TestFailure.h>
#include <cppunit/portability/Stream.h>

class ProgressListener: public CPPUNIT_NS::TestListener {
public:

	ProgressListener() :
	m_lastTestFailed(false) {
	}

	~ProgressListener() {
	}

	void startTest(CPPUNIT_NS::Test *test) {
		CPPUNIT_NS::stdCOut() << test->getName();
		CPPUNIT_NS::stdCOut() << "\n";
		CPPUNIT_NS::stdCOut().flush();

		m_lastTestFailed = false;
	}

	void addFailure(const CPPUNIT_NS::TestFailure &failure) {
		CPPUNIT_NS::stdCOut() << " : "
		<< (failure.isError() ? "error" : "assertion");
		m_lastTestFailed = true;
	}

	void endTest(CPPUNIT_NS::Test *test) {
		if (!m_lastTestFailed)
		CPPUNIT_NS::stdCOut() << " : OK";
		CPPUNIT_NS::stdCOut() << "\n";
	}

private:
	// Prevents the use of the copy constructor.
	ProgressListener(const ProgressListener &copy);

	// Prevents the use of the copy operator.
	void operator=(const ProgressListener &copy);

private:
	bool m_lastTestFailed;
};

int main() {
	// Create the event manager and test controller
	CPPUNIT_NS::TestResult controller;

	// Add a listener that collects test result
	CPPUNIT_NS::TestResultCollector result;
	controller.addListener(&result);

	// Add a listener that print dots as test run.
	ProgressListener progress;
	controller.addListener(&progress);

	// Add the top suite to the test runner
	CPPUNIT_NS::TestRunner runner;
	runner.addTest(CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest());
	runner.run(controller);

	// Print test in a compiler compatible format.
	CPPUNIT_NS::CompilerOutputter outputter(&result, CPPUNIT_NS::stdCOut());
	outputter.write();

	return result.wasSuccessful() ? 0 : 1;
}

#endif
